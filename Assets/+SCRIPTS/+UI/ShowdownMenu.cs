using TMPro;

[System.Serializable]
public class ShowdownMenu : Menu
{
    private static BattleTeam _currentAttacker;
    private static BattleTeam _currentDefender;
    public TextMeshProUGUI attackerResources;
    public TextMeshProUGUI defenderResources;

    public static void SetTeams(BattleTeam attacker, BattleTeam defender)
    {
        _currentAttacker = attacker;
        _currentDefender = defender;

    }

    public override void On()
    {
        base.On();
        //gameObject.SetActive(true);
   
        MenuManager.ClearDisplayText();
        MenuManager.DisplayText("You see a horde ahead...");
    }

 

    public static void Retreat()
    {
        MenuManager.OpenMenu(MenuType.main);
    }

    void Update()
    {
        attackerResources.text = _currentAttacker.teamName + _currentAttacker.Loot.GetResourcesText();
        defenderResources.text = _currentDefender.teamName + _currentDefender.Loot.GetResourcesText();
    }

  
}