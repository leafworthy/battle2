using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class ShrineProgressDisplay:MonoBehaviour
{
    [SerializeField]private Slider shrineBuildProgress;
    [SerializeField]private int shrineBuildTime;
    private void Start()
    {
        shrineBuildProgress = GetComponent<Slider>();
    }

    private void Update()
    {
        DisplayShrineBuildProgress();
    }

    private void DisplayShrineBuildProgress()
    {
        shrineBuildProgress.minValue = 0;
        shrineBuildProgress.value = PlayerResourcesManager.GetAmount(ResourceType.shrineProgress);
        shrineBuildProgress.maxValue = shrineBuildTime;
    }
}