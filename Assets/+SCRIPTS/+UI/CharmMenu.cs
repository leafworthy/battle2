using System;
using UnityEngine;

[System.Serializable]
public class CharmMenu : Menu
{
    public ButtonControl CharmButton;
    private int charmedToday = 0;
    private int attemmptsToday = 0;

    public override void On()
    {
        base.On();
        CharmButton.OnScore01 += Charm;
        charmedToday = 0;
        attemmptsToday = 0;
        DisplayWilling();
    }

    private void DisplayWilling()
    {
        MenuManager.ClearDisplayText();

        MenuManager.DisplayText("There are " + PlayerResourcesManager.GetAmount(ResourceType.spee) +
                              " people willing to listen today.");
    }

    private void Update()
    {
        CharmButton.disabled = PlayerResourcesManager.GetAmount(ResourceType.spee) < 1;
    }

    public override void Off()
    {
        base.Off();
        CharmButton.OnScore01 -= Charm;
    }

    public void Charm(bool score)
    {
        if (score)
        {
            PlayerResourcesManager.PlayerGains(new Resource(ResourceType.follower, 1, 100));
        }

        PlayerResourcesManager.PlayerLoses(new Resource(ResourceType.spee, 1, 1));
        if (score)
        {
            charmedToday++;
        }

        attemmptsToday++;
        DisplayWilling();
        DisplayCharmed();
    }

    private void DisplayCharmed()
    {
        MenuManager.DisplayText("\nFollowers Charmed Today: " + charmedToday + " / " + attemmptsToday);
    }
}