using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Menu : MonoBehaviour
{
    public string Name = "Untitled Menu";
    public GameObject ButtonLayoutGroup;
    public ResourceButtonsSO MenuButtonData;
    public TextMeshProUGUI MenuNameText;
    public TextMeshProUGUI InfoText;
    public TextMeshProUGUI ResourcesText;


    private void OnValidate()
    {
        MenuNameText.text = Name;
    }

    public virtual void On()
    {
        gameObject.SetActive(true);
        MenuNameText.text = Name;
        ClearButtons();
        SpawnButtons();
        
    }

    public virtual void Off()
    {
        ClearButtons();
        gameObject.SetActive(false);
    }

    private void Update()
    {
        Refresh();
    }

    private void SpawnButtons()
    {
        foreach (ResourceButtonData buttonData in MenuButtonData.ResourceButtons)
        {
            var newButton = GameObject.Instantiate(MenuButtonData.ButtonPrefab);
            var buttonScript = newButton.GetComponent<ResourceButton>();
            if (buttonScript != null)
            {
                buttonScript.Initialize(buttonData);
                newButton.transform.SetParent(ButtonLayoutGroup.gameObject.transform);
            }
        }
    }

    private void ClearButtons()
    {
        foreach (UnityEngine.Transform transform in ButtonLayoutGroup.transform)
        {
            if (transform == ButtonLayoutGroup.transform)
                break;
            GameObject.Destroy(transform.gameObject);
        }
    }

    public void Refresh()
    {
        if (ResourcesText != null)
        {
            MenuManager.DisplayResources();
        }

        if(InfoText!= null)
            InfoText.text = MenuManager.GetDisplayText();
    }
}