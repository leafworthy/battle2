using TMPro;

[System.Serializable]
public class MainMenu: Menu
{
    public override void On()
    {
        base.On();
        DisplayMainMenuText();
    }

    public static void DisplayMainMenuText()
    {
        MenuManager.ClearDisplayText();
        MenuManager.DisplayText(DayManager.GetNightText());
        MenuManager.DisplayText(DayManager.GetTodayText());
    }

    
}