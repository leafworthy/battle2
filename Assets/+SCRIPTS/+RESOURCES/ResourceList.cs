using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

[System.Serializable]
public class ResourceList : Dictionary<ResourceType, Resource>
{

    public string GetResourcesText()
    {
        var newString = "\nResources:";
        foreach (KeyValuePair<ResourceType, Resource> resource in this)
        {
            newString += resource.Value.GetResourceTextString();
        }

        return newString;
    }

    public string GetLootResourcesText()
    {
        var newString = "\nLoot:";
        foreach (KeyValuePair<ResourceType, Resource> resource in this)
        {
            if (resource.Value.IsValidResource())
            {
                newString += resource.Value.GetResourceTextString();
            }
        }

        return newString;
    }
    public void Lose(Resource lostResource)
    {
        TryGetValue(lostResource.type, out var existingResource);
        if (existingResource == null)
        {
            Debug.Log("lost resource that doesn't exist: " +lostResource.type.ToString());
            return;
        }

        Debug.Log("Losing resource: "
                  + lostResource.type.ToString()
                  + " by " + lostResource.amount
                  + "max: " + lostResource.max
                  + "from current resource: " + existingResource.type.ToString()
                  + " at amount: " + existingResource.amount
                  + " with max: " + existingResource.max);
        existingResource.amount -= lostResource.amount;
        existingResource.amount = Mathf.Max(existingResource.amount, 0);
    }

    public void Lose(ResourceList lostResources)
    {
        foreach (KeyValuePair<ResourceType, Resource> resource in lostResources)
        {
            Lose(resource.Value);
        }
    }

    public void Lose(ResourceType type, int amount)
    {
        Lose(new Resource(type, amount, amount));
    }

    public void Gain(Resource gainedResource)
    {
       
        TryGetValue(gainedResource.type, out var existingResource);
        if (existingResource == null)
        {
            // Debug.Log("Gaining new resource: "
            //           + gainedResource.type.ToString()
            //           + " by " + gainedResource.amount
            //           + "max: " + gainedResource.max);
            AddNewResource(gainedResource);
            return;
        }
        else
        {
            // Debug.Log("Gaining resource: "
            //           + gainedResource.type.ToString()
            //           + " by " + gainedResource.amount
            //           + "max: " + gainedResource.max
            //           + "from current resource: " + existingResource.type.ToString()
            //           + " at amount: " + existingResource.amount
            //           + " with max: " + existingResource.max);
            existingResource.AddTo(gainedResource);
        }
    }


    public void Gain(ResourceList gainedResources)
    {
        foreach (KeyValuePair<ResourceType, Resource> resource in gainedResources)
        {
            Gain(resource.Value);
        }
    }

    private void AddNewResource(ResourceType type, int amount)
    {
        AddNewResource(new Resource(type, amount, amount));
    }

    private void AddNewResource(Resource resource)
    {
        Add(resource.type, new Resource(resource.type, resource.amount, resource.max));
    }


    public int GetAmount(ResourceType type)
    {
        Resource existingResource;
        TryGetValue(type, out existingResource);
        return existingResource?.amount ?? 0;
    }

    
    public void SetAmount(ResourceType type, int amount)
    {
        TryGetValue(type, out var existingResource);
        if (existingResource == null)
        {
            return;
        }

        existingResource.amount = amount;
    }

    public ResourceList GetRandomized()
    {
        var newList = new ResourceList();
        foreach (var pair in this)
        {
            newList.Gain(pair.Value.RandomizeAmount());
        }

        return newList;
    }

    public void Gain(List<Resource> gainedResources)
    {
        foreach (Resource resource in gainedResources)
        {
            Gain(resource);
        }
    }

    public void GainValidResources(ResourceList resources)
    {
        foreach (var pair in resources)
        {
            if (pair.Value.IsValidResource())
            {
                Gain(pair.Value);
            }
        }
    }

    public ResourceList GetValidResources(ResourceList resources)
    {
        var newList = new ResourceList();
        foreach (var pair in resources)
        {
            if (pair.Value.IsValidResource())
            {
                newList.Gain(pair.Value);
            }
        }

        return newList;
    }

    public void SetMax(ResourceType type, int amount)
    {
        TryGetValue(type, out var existingResource);
        if (existingResource == null)
        {
            return;
        }

        existingResource.max = amount;
    }

}