using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "Resources Buttons", menuName = "ScriptableObjects/ResourceButtonSO", order = 1)]
public class ResourceButtonsSO : ScriptableObject
{
    public string Name;
    public GameObject ButtonPrefab;
    public List<ResourceButtonData> ResourceButtons;
}

[System.Serializable]
public class ResourceButtonData
{
    [SerializeField] public string ButtonText;
    [SerializeField] public Resource resourceToGain;
    [SerializeField] public Resource resourceToLose;

    [SerializeField]
    public ButtonType buttonType;
}

public enum ButtonType
{
    TradeResource,
    None,
    Charm,
    Sleep,
    Attack,
    Fight,
    Run,
    MainMenu,
    Back,
    Retreat,
    SellAll,
    Test
}