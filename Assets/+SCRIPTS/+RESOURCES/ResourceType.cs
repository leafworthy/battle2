public enum ResourceType
{
    none,
    spee,
    spoo,
    silver,
    gold,
    gem,
    shrine,
    maraud,
    follower,
    swordsman,
    biggie,
    shrineProgress,
    storage,
    wraith,
    bomber
}