using static PlayerResourcesManager;

internal class ResourceProducer
{
    private readonly int _shrineBuildTime = 3;
    private bool _shrineBuildingToday = false;
    private readonly int _spooPerFollower = 1;

    public string GetResourcesProducedText()
    {
        var resourcesString = "\nResources Produced: ";
        resourcesString += "\nSpoo Produced: " + GetSpooProducedToday().ToString();
        return resourcesString;
    }

    public void Produce()
    {
        FollowersProduceSpoo();
        BuildShrine();
        ResetMaraudAmount();
        ResetSpeeAmount();
        
    }

    private void BuildShrine()
    {
        if (!_shrineBuildingToday) return;

        PlayerGains(new Resource(ResourceType.shrineProgress, 1,1));

        if (GetAmount(ResourceType.shrineProgress) < _shrineBuildTime) return;
        SetAmount(ResourceType.shrineProgress, 0);
        PlayerGains(new Resource(ResourceType.shrine, 1,1));
        _shrineBuildingToday = false;
    }

    private void FollowersProduceSpoo()
    {
        PlayerGains(new Resource(ResourceType.spoo, GetSpooProducedToday(),
            GetSpooProducedToday()));
    }

    private int GetSpooProducedToday()
    {
        return GetAmount(ResourceType.follower) * _spooPerFollower *
               GetAmount(ResourceType.shrine);
    }

    private void ResetMaraudAmount()
    {
        SetAmount(ResourceType.maraud, 1);
    }

    private void ResetSpeeAmount()
    {
       SetAmount(ResourceType.spee, GetAmount(ResourceType.gold));
       SetMax(ResourceType.spee, GetAmount(ResourceType.gold));
    }
}