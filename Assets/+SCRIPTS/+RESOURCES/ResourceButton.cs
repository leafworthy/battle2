using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class ResourceButton : MonoBehaviour
{
    public static Action<ResourceButton> OnClickButton;
    [SerializeField] private ResourceButtonData _data;

    private Button _button;
    private int _currentCostTypeOwned;
    public TextMeshProUGUI buttonLabelText;
    public TextMeshProUGUI quantityToLoseText;
    public TextMeshProUGUI quantityToGainText;

    public ResourceButtonData Data
    {
        get => _data;
        private set => _data = value;
    }

    public ButtonType GetButtonType()
    {
        return Data.buttonType;
    }
    public void Initialize(ResourceButtonData data)
    {
        Data = data;

        buttonLabelText.text = Data.ButtonText;
        _button = GetComponent<Button>();
        _button.onClick.AddListener(() =>
        {
            OnClickButton?.Invoke(this);
        });
        if (Data.buttonType == ButtonType.TradeResource)
        {
            if (Data.resourceToGain.amount > 1)
            {
                quantityToGainText.text = Data.resourceToGain.GetCurrencyString();
            }
            else
            {
                quantityToGainText.enabled = false;
            }
        }
        else
        {
            quantityToGainText.enabled = false;
            quantityToLoseText.enabled = false;
        }
    }

    private void Update()
    {
        RefreshButton();
    }

    private void RefreshButton()
    {
        if (Data.resourceToLose.type != ResourceType.none)
        {
            _currentCostTypeOwned = PlayerResourcesManager.GetAmount(Data.resourceToLose.type);

            quantityToLoseText.text = Data.resourceToLose.GetCurrencyString();

            if (_button == null)
                return;
            if (_currentCostTypeOwned < Data.resourceToLose.amount)
            {
                _button.interactable = false;
            }
            else
            {
                _button.interactable = true;
            }
        }
    }
}