using System;
using UnityEngine;


public class DayManager: Singleton<DayManager>
{
    private static bool _nightAttack;
    private static int _dayNumber = 1;
    private const int MaxDays = 30;
    
    public static Action OnNightAttack;
    public static Action OnSleep;
    public static Action OnNightFinished;

    public void Start()
    {
        Reset();
        InputController.OnSleep += Sleep;
    }

    public void Reset()
    {
       _dayNumber = 1;
    }

    private static void Sleep()
    {
        OnSleep?.Invoke();
        DoNightAttacks();
    }
    

    private static void NightFinished()
    {
        _dayNumber++;
        OnNightFinished?.Invoke();
    }
    
    private static void DoNightAttacks()
    {
        if (GetsMarauded())
        {
            _nightAttack = true;
            OnNightAttack?.Invoke();
        }
        else
        {
            _nightAttack = false;
            NightFinished();
        }
    }

    private static bool GetsMarauded()
    {
        if (_dayNumber <= 1) return false;
        var fightsTonightRandom = UnityEngine.Random.Range(0, 10);
        return fightsTonightRandom == 0;

    }


    public static void OnBattleFinished()
    {
        if (_nightAttack)
        {
            NightFinished();
        }
    }

    public static string GetTodayText()
    {
        string returnString = "Day: " + _dayNumber + " / " + MaxDays;
        switch (_dayNumber)
        {
            case 1:
                returnString += "\nYou died today. Welcome to your life as a wraith.";
                break;
            case 2:
                returnString += "\nYou survived your first night.";
                break;
            default:
                break;
        }

        MenuManager.DisplayText(BattleManager.GetLatestResults());
        MenuManager.DisplayText(PlayerResourcesManager.GetResourcesProducedText());
        return returnString;
    }

    public static string GetNightText()
    {
        return _nightAttack ? "\nYou were attacked in the night." : "\nIt was a normal night.";
    }
}