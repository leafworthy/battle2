using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Serialization;
using Debug = UnityEngine.Debug;

[System.Serializable]
public class PlayerResourcesManager : Singleton<PlayerResourcesManager>
{
    //Resource.cs
    [SerializeField] private ResourceDefaultsSO resourceDefaultsSo;
    private static ResourceList _playerResources = new ResourceList();
    private static ResourceProducer _resourceProducer = new ResourceProducer();
    [SerializeField] public Color PlayerColor { get; set; }

    private void Start()
    {
        PlayerGains(resourceDefaultsSo.DefaultPlayerResources.GetResourceList());
        BattleManager.OnBattleResults += PlayerGainsAndLoses;
        DayManager.OnNightFinished += OnNightFinished;
        InputController.OnStartBattle += OnStartBattleButtonPressed;
        InputController.OnSellAll += OnSellAllButtonPressed;
    }

    private void OnSellAllButtonPressed(Resource gain, Resource lose)
    {
        SellAll(gain, lose);
    }

    private void OnStartBattleButtonPressed()
    {
        PlayerLoses(new Resource(ResourceType.maraud, 1, 1));
    }


    private static void OnNightFinished()
    {
        _resourceProducer.Produce();
    }


    public static void Trade(Resource resourceToGain, Resource resourceToLose)
    {
        PlayerGains(resourceToGain);
        PlayerLoses(resourceToLose);
    }


    public static void PlayerLoses(Resource lostResource)
    {
        _playerResources.Lose(lostResource);
    }

    public static void PlayerLoses(ResourceList lostResources)
    {
        _playerResources.Lose(lostResources);
    }

    public static void PlayerGains(Resource gainedResource)
    {
        _playerResources.Gain(gainedResource);
    }


    public static void PlayerGains(ResourceList gainedResources)
    {
        _playerResources.Gain(gainedResources);
    }

    public static int GetAmount(ResourceType type)
    {
        return _playerResources.GetAmount(type);
    }

    public static void SetAmount(ResourceType type, int amount)
    {
        _playerResources.SetAmount(type, amount);
    }

    public static string GetPlayerResourcesText()
    {
        var resourcesText = _playerResources.GetResourcesText();
        return resourcesText;
    }


    public static ResourceList GetRandomPlayerLoot()
    {
        var list = new ResourceList();
        list.Gain(_playerResources.GetRandomized());
        return list;
    }

    public static ResourceList GetPlayerLoot()
    {
        var list = new ResourceList();
        foreach (KeyValuePair<ResourceType, Resource> resource in _playerResources)
        {
            list.Gain(resource.Value);
        }

        return list;
    }

    private static void PlayerGainsAndLoses(BattleResults results)
    {
        if (results.Winner.isHuman)
        {
            PlayerGains(results.ResourcesGained);
        }

        PlayerLoses(results.ResourcesLost);
    }

    public static ResourceList GetEnemyLoot(bool playerIsAttacker)
    {
        var list = new ResourceList();
        if (playerIsAttacker)
        {
            list.Gain(I.resourceDefaultsSo.MaxDefenderResourcesScriptableObject.Resources.GetRandomized());
        }
        else
        {
            list.Gain(I.resourceDefaultsSo.MaxAttackerResourcesScriptableObject.Resources.GetRandomized());
        }

        return list;
    }


    public static void SetMax(ResourceType type, int amount)
    {
        _playerResources.SetMax(type, amount);
    }

    public static void SellAll(Resource resourceToGain, Resource resourceToLose)
    {
        var amountToSell = GetAmount(resourceToLose.type);
        for (var i = 0; i < amountToSell; i++)
        {
            PlayerLoses(resourceToLose);
            PlayerGains(resourceToGain);
        }
    }

    public static string GetResourcesProducedText()
    {
        return _resourceProducer.GetResourcesProducedText();
    }
}