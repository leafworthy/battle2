using System;
using UnityEngine;

public class InputController : Singleton<InputController>
{
    private void Start()
    {
        ResourceButton.OnClickButton += OnButtonPress;
        BattleManager.OnBattleFinished += DayManager.OnBattleFinished;
        DayManager.OnNightAttack += BattleManager.OnNightAttack;
    }

    public static Action<Resource, Resource> OnTrade;
    public static Action OnSleep;
    public static Action OnCharm;
    public static Action OnShowdown;
    public static Action OnMainMenu;
    public static Action OnRetreat;
    public static Action<Resource, Resource> OnSellAll;
    public static Action OnStartBattle;
    public static Action<BattleTeam, BattleTeam> OnStartTestBattle;
    

    private void OnButtonPress(ResourceButton button)
    {
        switch (button.GetButtonType())
        {
            case ButtonType.TradeResource:
                OnTrade?.Invoke(button.Data.resourceToGain, button.Data.resourceToLose);
                break;
            case ButtonType.Sleep:
                OnSleep?.Invoke();
                break;
            case ButtonType.Charm:
                OnCharm?.Invoke();
                break;
            case ButtonType.Attack:
                OnShowdown?.Invoke();
                break;
            case ButtonType.MainMenu:
                OnMainMenu?.Invoke();
                break;
            case ButtonType.Retreat:
                OnRetreat?.Invoke();
                break;
            case ButtonType.SellAll:
                OnSellAll?.Invoke(button.Data.resourceToGain, button.Data.resourceToLose);
                break;
            case ButtonType.Fight:
                OnStartBattle?.Invoke();
                break;
            case ButtonType.Test:
                Debug.Log("test press");
                OnStartTestBattle?.Invoke(BattleTester.GetAttacker(),BattleTester.GetDefender());
                break;

                

            default:
                break;
        }
    }

}