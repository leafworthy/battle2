﻿// ReSharper disable once RedundantUsingDirective

using System;
using UnityEngine;
using UnityEngine.Serialization;

public enum MenuType
{
    main,
    battle,
    charm,
    showdown
}

public class MenuManager : Singleton<MenuManager>
{
    public MainMenu mainMenu;
    public CharmMenu charmMenu;
    public ShowdownMenu showdownMenu;
    public BattleMenu battleMenu;

    private static Menu _currentMenu;
    private static string _textString = "";


    void Awake()
    {
        DisableMenuGameObjects();
        OpenMenu(MenuType.main);
        InputController.OnMainMenu += () => OpenMenu(MenuType.main);
       
    }
    public void FixedUpdate()
    {
        if (_currentMenu != null)
        {
            _currentMenu.Refresh();
        }
    }

    public static string GetDisplayText()
    {
        return _textString;
    }

    public static void ClearDisplayText()
    {
       
        _textString = "";
        Debug.Log("<color=red>cleared</color>");
    }

    public static void DisplayText(string message)
    {
        
        _textString += message;
        Debug.Log(message);
    }

    private static void ShowMenu(Menu menuToShow)
    {
        if (_currentMenu != null)
        {
            _currentMenu.Off();
        }

        _currentMenu = menuToShow;
        _currentMenu.On();
    }


    public static void OpenMenu(MenuType type)
    {
        switch (type)
        {
            case MenuType.main:
                ShowMenu(I.mainMenu);
                break;
            case MenuType.battle:
                ShowMenu(I.battleMenu);
                break;
            case MenuType.charm:
                ShowMenu(I.charmMenu);
                break;
            case MenuType.showdown:
                ShowMenu(I.showdownMenu);
                break;
            default:
                break;
        }
    }


    public static void DisplayResources()
    {
        _currentMenu.ResourcesText.text = PlayerResourcesManager.GetPlayerResourcesText();
    }
    

    private static void DisableMenuGameObjects()
    {
        foreach (Menu menu in I.GetComponentsInChildren<Menu>())
        {
            menu.gameObject.SetActive(false);
        }
    }


}