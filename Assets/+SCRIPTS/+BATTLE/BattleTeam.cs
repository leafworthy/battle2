using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

[System.Serializable]
public class BattleTeam
{
    public BattleTeam(bool isAttacker, bool isHuman, ResourceList loot)
    {
        this.isAttacker = isAttacker;
        this.isHuman = isHuman;
        Loot.Gain(loot);
        if (isHuman)
        {
            color = PlayerResourcesManager.I.PlayerColor;
            teamName = "Human";
        }
        else
        {
            color = Color.red;
            teamName = "Enemy";
        }
    }

    public string teamName;
    public Action<BattleTeam> OnTeamDie;
    public bool isAttacker;
    public bool isHuman;
    public Color color = Color.white;
    private CombatUnit _leader;

    public CombatUnit Leader
    {
        get => _leader;
        set => SetLeader(value);
    }

    private BattleTeam _opponent;

    private void SetLeader(CombatUnit leader)
    {
        _leader = leader;
    }

    public void SetOpponent(BattleTeam opponent)
    {
        _opponent = opponent;
    }
    public ResourceList Loot = new ResourceList();
    public List<Resource> startingLoot = new List<Resource>();

    private List<CombatUnit> _currentUnits = new List<CombatUnit>();

    private void OnUnitDie(CombatUnit unit)
    {
//        Debug.Log((unit.GetOwner().isHuman?"Player":"Enemy") + "'s Living Units: " + getLivingUnitsAmount());
        unit.onDie -= OnUnitDie;
        if (GetLivingUnitsAmount() <= 0f)
        {
            //Debug.Log("on team die");
            OnTeamDie?.Invoke(this);
        }
    }

    public ResourceList GetDeadUnits()
    {
        var currentDeadUnits = new ResourceList();
        foreach (var unit in _currentUnits)
        {
            if (unit.Dead)
            {
               // Debug.Log("Dead Unit Added: " + unit.unitType.ToString());
                currentDeadUnits.Gain(new Resource(unit.unitType, 1, 100));
            }
        }

        return currentDeadUnits;
    }


    public List<CombatUnit> GetUnits()
    {
        return _currentUnits;
    }


    public int GetLivingUnitsAmount()
    {
        return _currentUnits.Count(t => !t.Dead);
    }


    public BattleTeam GetOpponent()
    {
        return _opponent;
    }

    public void SpawnUnits()
    {
        //Debug.Log("Spawning units for: " + this.ToString());
        _currentUnits = new List<CombatUnit>();
        _currentUnits = Spawner.I.SpawnPlayerUnits(this);
        foreach (var currentUnit in _currentUnits)
        {
//            Debug.Log(currentUnit.unitType.ToString());
            currentUnit.onDie += OnUnitDie;
        }
    }

    public void Retreat()
    {
        foreach (CombatUnit unit in _currentUnits)
        {
            unit.DoAction(Actions.Retreating);
        }

        _leader.DoAction(Actions.Retreating);

    }

    public void Cheer()
    {
        foreach (CombatUnit unit in _currentUnits)
        {
            unit.DoAction(Actions.Cheering);

        }

        _leader.DoAction(Actions.Cheering);
    }

    public void StartToCastSpell(ISpell spell)
    {
        //
    }
}