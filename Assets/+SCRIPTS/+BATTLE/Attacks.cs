using System.Linq;
using UnityEngine;



public class Attack : MonoBehaviour
{
    public float damageAmount;
    public bool statusEffect = false;
    private CombatUnit _target;
    private bool _inProgress = false;

    private bool CanAttack()
    {
        return !_inProgress;
    }

    public void StartAttack(CombatUnit target)
    {
        if (CanAttack())
        {
            _target = target;
            _inProgress = true;
        }
    }

    public bool IsFinished()
    {
        return true;
    }
}

[RequireComponent(typeof(CombatUnit))]
public class Attacks : MonoBehaviour
{
    private CombatUnit _unit;
    private CombatUnit _enemyTarget;
    private Attack _currentAttack;
    private CombatUnit _currentTarget;
    private void Awake()
    {
        _unit = GetComponent<CombatUnit>();
    }

    public Actions GetRandomAttackAction()
    {
        var randomIndex = Random.Range(0, 6);
            switch (randomIndex)
            {
                case 1:
                    return Actions.Attack1;
                case 2:
                    return Actions.Attack2;
                case 3:
                    return Actions.Attack3;
                case 4:
                    return Actions.Attack4;
                case 5:
                    return Actions.SpecialAttack;
            }
            return Actions.Attack1;
    }


    public void StartAttack(Attack attack, CombatUnit target)
    {
    if(_currentAttack.IsFinished())
        attack.StartAttack(target);
    }
    public void AttackHitTarget()
    {
        if (!IsTargetValidEnemy(Target)) return;
        Target.TakeDamage(_currentAttack, _unit);
    }

    private CombatUnit GetTarget()
    {
        return _currentTarget == null ? FindClosestTarget(true) : _currentTarget;
    }

    
    public CombatUnit Target
    {
        get => GetTarget();
        set => SetTarget(value);
    }

    private void SetTarget(CombatUnit target)
    {
        if (target == null)
            return;
        _currentTarget = target;
    }
    private bool IsTargetValidEnemy(CombatUnit combatUnit)
    {
        if (_currentTarget == null)
            return false;
        if (_currentTarget.unitType == ResourceType.wraith)
            return false;

        return (_currentTarget != null && !_currentTarget.Dead && (_currentTarget.unitType != ResourceType.wraith) && (_currentTarget.Team != _unit.Team));
    }

    private CombatUnit FindClosestTarget(bool enemy)
    {
        if (enemy)
        {
            var closestUnits = Spawner.units.Where(t => ((t.Team != _unit.Team) &&
                                                         (t.unitType != _unit.unitType)));
            var closestFriendlyUnit = closestUnits.OrderBy(t =>
                Vector2.Distance(transform.position, t.transform.position)).FirstOrDefault();
            return closestFriendlyUnit;
        }
        else
        {
            var closestEnemyUnit = Spawner.units.Where(t => IsTargetValidEnemy(t))
                .OrderBy(t => Vector2.Distance(transform.position, t.transform.position))
                .FirstOrDefault();
            return closestEnemyUnit;
        }
    }

    public void HitAllEnemies()
    {
        var units = _unit.Team.GetOpponent().GetUnits();
        foreach (var combatUnit in units)
        {
            if (!combatUnit.Dead)
            {
                combatUnit.TakeDamage(_currentAttack, _unit);
            }
        }
    }

}
