using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Movement))]
[ExecuteInEditMode]
public class Health: MonoBehaviour
{
        public float healthAmount = 100;
        public float healthMax = 100;
        public bool dead = false;
        public Bar healthBar;
        [FormerlySerializedAs("HitByLightning")] public Action OnHitByLightning;
        public Action OnDie;
        private Movement _move;

        private void Start()
        {
                _move = GetComponent<Movement>();
                healthBar.UpdateBar(0,healthMax);
        }


        
        private void OnValidate()
        {
                healthBar = GetComponentInChildren<Bar>();
        }

        public void TakeDamage(Attack attack, CombatUnit attacker)
        {
                if (!dead)
                {
                        healthAmount -= attack.damageAmount;
                        _move.Push(attack.damageAmount, attacker.transform);
                        if (healthAmount <= 0)
                        {
                                if (!dead)
                                {
                                        dead = true;
                                        healthBar.gameObject.SetActive(false); 
                                      OnDie?.Invoke();
                                }
                        }
                        else
                        {
                                if (attack.statusEffect)
                                {
                                        StatusEffect();
                                }
                                healthBar.UpdateBar(healthMax - healthAmount, healthMax);
                        }
                }
                else
                {
                        healthBar.gameObject.SetActive(false); 
                }
        }

        public void StatusEffect()
        {
                OnHitByLightning?.Invoke();
        }
}