﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class HideRevealObjects : MonoBehaviour
{
    [SerializeField] private List<GameObject> objectsToReveal = new List<GameObject>();

    [Range(0,20)]
    [SerializeField] private int numberToReveal;

    public GameObject AnotherTransform;
    

    // Start is called before the first frame update
    private void OnValidate()
    {
        Refresh();
    }

    public void SetReveal(int number)
    {
        numberToReveal = number;
        Refresh();
    }

    private void Refresh()
    {
        objectsToReveal.Clear();
        if (AnotherTransform != null)
        {
            foreach (UnityEngine.Transform child in AnotherTransform.transform)
            {
                if (child != AnotherTransform.transform)
                {
                    objectsToReveal.Add(child.gameObject);
                }
            }
        }

        foreach (UnityEngine.Transform child in transform)
        {
            if (child != transform)
            {
                objectsToReveal.Add(child.gameObject);
            }
        }

        //numberToReveal = UnityEngine.Random.Range(0, objectsToReveal.Count);
        numberToReveal = Mathf.Clamp(numberToReveal, 0, objectsToReveal.Count);
        for (int i = 0; i < objectsToReveal.Count; i++)
        {
            if (i < numberToReveal)
            {
                objectsToReveal[i].SetActive(true);
            }
            else
            {
                objectsToReveal[i].SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}