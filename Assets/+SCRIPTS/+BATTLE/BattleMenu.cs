using System;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;

[System.Serializable]
public class BattleMenu : Menu
{
    [SerializeField] private TextMeshProUGUI attackerResources;
    [SerializeField] private TextMeshProUGUI defenderResources;

    public override void On()
    {
        base.On();
        RefreshResourcesText();
    }

    void FixedUpdate()
    {
        RefreshResourcesText();
    }

    private void RefreshResourcesText()
    {
        attackerResources.text = BattleManager.Attacker.Loot.GetLootResourcesText();
        defenderResources.text = BattleManager.Defender.Loot.GetLootResourcesText();
    }
}