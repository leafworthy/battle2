﻿using System;
using UnityEngine;

public class Movement : MonoBehaviour
{

    public float closeEnoughDistance = .7f;
    public float walkSpeed = 2;

    private readonly float _pushFactor = .10f;
    private readonly float _minimumVelocity = .1f;
    private readonly float _inirtiaFactor = .7f;

    private Vector2 _velocity = Vector2.zero;

    private GameObject _targetPositionObject;
    public Transform clickTarget;
    private Vector2 _targetPosition;
    private bool _clickMoving = false;

    private bool _moving = false;
    private bool _on = true;
    private CombatUnit _unit;
    [SerializeField]private GameObject scaleObject;

    private void Awake()
    {
        _targetPositionObject = new GameObject();
        _unit = GetComponent<CombatUnit>();
        _unit.onDie += Die;
    }

    private void FixedUpdate()
    {
        UpdateVelocity();
    }

    public virtual void Die(CombatUnit combatUnit)
    {
        _on = false;
    }

    protected virtual void Update()
    {

        if (_clickMoving)
        {
            if (clickTarget != null)
            {
                if (!IsCloseEnoughToPosition(clickTarget.transform.position))
                {
                    MoveTowardPosition(clickTarget.transform.position);
                    FaceTarget();
                }
                else
                {
                    SetMoving(false);
                }
            }
        }
        else if (_moving)
        {
            if (!IsCloseEnoughToTarget())
            {
                MoveTowardTarget();
                FaceTarget();
            }
            else
            {
                SetMoving(false);
            }
        }
    }

    private void UpdateVelocity()
    {
        if (!_on)
            return;
        if (_velocity != Vector2.zero && _velocity.magnitude <= _minimumVelocity)
        {
            _velocity = Vector2.zero;
        }
        else
        {
            _velocity *= _inirtiaFactor;
            transform.position += (Vector3) _velocity * Time.deltaTime;
        }
    }

   

    public void SetTargetPosition(Vector2 position)
    {
        if (_targetPositionObject == null)
        {
            _targetPositionObject = new GameObject();
        }
        _targetPositionObject.transform.position = position;
    }

    public void SetTargetUnit(CombatUnit targetUnit)
    {
        if (targetUnit == null)
            return;
        _targetPositionObject = targetUnit.gameObject;
    }

public bool IsCloseEnoughToTarget()
    {
        
        if (_targetPositionObject == null)
            return false;
        var distance = Vector2.Distance(gameObject.transform.position, _targetPositionObject.transform.position);
        return distance <= closeEnoughDistance;
    }

    public bool IsThisCloseToPoint(float closeEnough, Vector2 point)
    {
      
        var distance = Vector2.Distance(gameObject.transform.position, point);
        return distance <= closeEnough;
    }

    public bool IsCloseEnoughToPosition(Vector2 point)
    {
        var distance = Vector2.Distance(gameObject.transform.position, point);
        return distance <= closeEnoughDistance;
    }

    private void MoveTowardTarget()
    {
        var targetDir = (_targetPositionObject.transform.position - transform.position).normalized;
        transform.position += (Vector3) targetDir * (walkSpeed * Time.deltaTime);
    }

    private void MoveTowardPosition(Vector2 target)
    {
        var targetDir = (Vector2)(target - (Vector2) transform.position).normalized;
        transform.position += (Vector3) targetDir * (walkSpeed * Time.deltaTime);
    }

    private void SetTargetToSide(bool toRight)
    {
        var toRightMultiplier = (toRight) ? 1 : -1;
        SetTargetPosition(new Vector2(transform.position.x + toRightMultiplier * 100, transform.position.y));
    }

    public void SetClickMoving(bool move)
    {
        _clickMoving = move;
    }

    public void SetMoving(bool move)
    {
        _moving = move;
    }

    private void SetXScale(int scale)
    {
        var localScale = scaleObject.transform.localScale;
        localScale = new Vector3(Mathf.Abs(localScale.x) * scale, localScale.y, localScale.z);
        transform.localScale = localScale;
    }

    private void FaceTarget()
    {
        if (_targetPositionObject.transform.position.x > transform.position.x)
        {
            //FACE RIGHT
            SetXScale(1);
        }
        else
        {
            //FACE LEFT
            SetXScale(-1);
        }
    }


    public void Push(float pushAmplitude, Transform pusher)
    {
        var dir = (transform.position - pusher.position).normalized;
        _velocity += (Vector2) dir * (pushAmplitude * _pushFactor);
    }

    public void Retreat(BattleTeam owner)
    {
        SetTargetToSide(!owner.isAttacker);
        SetMoving(true);
    }

    public bool MoveToTarget()
    {

        if (!CanMoveToTarget())
        {
            return false;
        }
        else
        {
            SetMoving(true);
            return true;
        }
    }

    private bool CanMoveToTarget()
    {
        return !IsCloseEnoughToTarget();
    }


}