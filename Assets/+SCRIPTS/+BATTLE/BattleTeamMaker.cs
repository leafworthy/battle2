using UnityEngine;

public static class BattleTeamMaker 
{
 

    public static BattleTeam CreatePlayerDefenderTeam()
    {
        return new BattleTeam(false, true, PlayerResourcesManager.GetPlayerLoot());
    }
    
    public static BattleTeam CreatePlayerAttackerTeam()
    {
        return new BattleTeam(true, true, PlayerResourcesManager.GetPlayerLoot());
    }

    public static BattleTeam CreateNewMaraudAttacker()
    {
        return new BattleTeam(true, false, PlayerResourcesManager.GetEnemyLoot(false));
       
    }

    public static BattleTeam CreateNewMaraudDefender()
    {
        return new BattleTeam(false, false, PlayerResourcesManager.GetEnemyLoot(true));

    }
}