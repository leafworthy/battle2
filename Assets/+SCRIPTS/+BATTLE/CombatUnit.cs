﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CombatUnit : MonoBehaviour
{
    
    private ActionsManager _actionsManager;
    private Health _health;
    public Action<CombatUnit> onDie;
    public BattleTeam Team { get; set; }
    public bool Dead => _health.dead;
    public ResourceType unitType;

    
    void Awake()
    {
        _health = GetComponent<Health>();
        _health.OnDie += OnHealthDie;
        _actionsManager = GetComponent<ActionsManager>();
    }


    public void TakeDamage(Attack attack, CombatUnit attacker)
    {
        _health.TakeDamage(attack, attacker);
        
    }

    //CALLED FROM ANIMATION  
    public void Think()
    {
        if (Dead) return;
    }
    
    public void OnDeathAnimationFinished()
    {
        //Destroy(gameObject);
    }
    
    private void OnHealthDie()
    {
        Destroy(GetComponent<Rigidbody2D>());
        Destroy(GetComponent<CircleCollider2D>());
        onDie?.Invoke(this);
    }

    public void DoAction(Actions newAction)
    {
        _actionsManager.DoAction(newAction);
    }

}