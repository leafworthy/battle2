using System;
using UnityEngine;

internal class AnimationManager:MonoBehaviour
{
    private Animator _anim;
    private CombatUnit _unit;
    private ActionsManager _actions;

    private void Start()
    {
        _anim = GetComponent<Animator>();
        _unit = GetComponent<CombatUnit>();
        _actions = GetComponent<ActionsManager>();
        _unit.onDie += OnDie;
        _actions.onActionChange += UpdateAction;
    }

    private void UpdateAction(Actions action)
    {
        Debug.Log("action updated");
       SetAnimationParameter(action);
    }

    private void OnDie(CombatUnit unit)
    {
        ResetAllBools();
        _anim.SetBool("Dead", true);
    }

    private void ResetAllBools()
    {
        string[] ActionTypeNames = System.Enum.GetNames(typeof(Actions));
        for (int i = 0; i < ActionTypeNames.Length; i++)
        {
            if (HasParameter(ActionTypeNames[i]))
            {
                _anim.SetBool(ActionTypeNames[i], false);
            }
        }
    }

    private void SetAnimationParameter(Actions action)
    {
        ResetAllBools();
        if (HasParameter(action.ToString()))
        {
            _anim.SetBool(action.ToString(), true);
        }
        else
        {
            
        }
    }

    private bool HasParameter(string paramName)
    {
        if (_anim == null)
            return false;
        foreach (AnimatorControllerParameter param in _anim.parameters)
        {
            if (param.name == paramName)
                return true;
        }

        Debug.Log("don't have that parameteter" + paramName);
        
        return false;
    }

}