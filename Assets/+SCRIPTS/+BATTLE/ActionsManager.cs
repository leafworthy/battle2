using System;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

internal class ActionsManager : MonoBehaviour
{
    private Actions _currentAction;
    public TMP_Text actionLabel;
    public Actions CurrentAction
    {
        get => _currentAction;
        set => SetAction(value);
    }

    private bool _retreating = false;
    private bool _cheering = false;
    private CombatUnit _unit;
    private Movement _move;

    public Action<Actions> onActionChange;
    private bool _clickMoving;
    private Attacks _attacks;


    private void Start()
    {
        _attacks = GetComponent<Attacks>();
        _unit = GetComponent<CombatUnit>();
        _move = GetComponent<Movement>();
    }

    void Update()
    {
        actionLabel.text = CurrentAction.ToString();
        if (!_unit.Dead)
        {
            DoCurrentAction();
        }
    }

    private void DoCurrentAction()
    {
        switch (CurrentAction)
        {
            case Actions.Running:
                if (!_move.MoveToTarget())
                {
                    GetNewAction();
                }
                break;
            case Actions.Cheering:
                _move.SetMoving(false);
                break;
           
        }
    }

    private void SetAction(Actions action)
    {
        if (!_unit.Dead)
        {
            _currentAction = action;
            onActionChange?.Invoke(action);
        }
        else
        {
            _currentAction = Actions.Dead;
        }
    }

    protected virtual Actions GetNextAction()
    {
        if (_retreating)
        {
            
            return Actions.Running;
        }

        if (_cheering)
        {
            return Actions.Cheering;
        }

        return Think();
    }

  

    private Actions Think()
    {
        return _move.IsCloseEnoughToTarget() ? _attacks.GetRandomAttackAction() : Actions.Running;
    }


    protected void DoRetreat()
    {
        _move.Retreat(_unit.Team);
        _retreating = true;
        _cheering = false;
        SetAction(Actions.Running);
    }

    private void DoCheer()
    {
        _cheering = true;
        SetAction(Actions.Cheering);
    }


    private void GetNewAction()
    {
        SetAction(GetNextAction());
    }

    public void DoAction(Actions action)
    {
        SetAction(action);
        switch (action)
        {
            case Actions.Attack1:
                break;
            case Actions.Attack2:
                break;
            case Actions.Attack3:
                break;
            case Actions.Attack4:
                break;
            case Actions.SpecialAttack:
                break;
            case Actions.Running:
                break;
            case Actions.Cheering:
                DoCheer();
                break;
            case Actions.Retreating:
                DoRetreat();
                break;
            case Actions.Standing:
                break;
            case Actions.Dead:
                break;
            case Actions.ClickMoving:
                _clickMoving = true;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(action), action, null);
        }
    }
}